# How to Add Cypress for Quick Accessibility Testing

## Official Docs

Cypress Docs (Install): https://docs.cypress.io/guides/getting-started/installing-cypress.html#System-requirements

Cypress Docs (First Steps): https://docs.cypress.io/guides/getting-started/writing-your-first-test.html#Add-a-test-file

Cypress Axe Docs: https://github.com/component-driven/cypress-axe


## npm packages to be installed at the root of the project:
```sh
# Cypress Dep
npm install --save-dev cypress

# Cypress Axe Deps
npm install --save-dev axe-core cypress-axe

# Concurrently for running the app and cypress in parallel
npm install --save-dev concurrently
```

## Scripts to add to your package.json 
```json
"scripts": {
  "start": "serve index.html",
  "e2e": "concurrently -k -n app,cypress \"npm run start\" \"sleep 5 && cypress run\"",
  "e2e:gui": "concurrently -k -n app,cypress \"npm run start\" \"cypress open\""
}
```


## Simple test to check for accessibility violations on a landing page
* This file will need to be added to `<project-root>/cypress/integration` 
* Cypress will automatically generate `<project-root>/cypress/*` when running for the first time 

```js
const url = "http://localhost:8000/";

/// <reference types="cypress" />

context("Accessibility", () => {
  beforeEach(() => {
    cy.visit(url);
    cy.injectAxe();
  });

  it("Has no detectable a11y violations on load", () => {
    cy.checkA11y();
  });
});

```

Note: You will also want to write tests for other pages in your app
as well as account for lazy loaded content


## Bonus, wire into husky (git hook tool)

Install husky:
```sh
npm install --save-dev husky
```

Create a `.huskyrc` file and add:
```json
{
  "hooks": {
    "pre-push": "npm run e2e"
  }
}
```

if you have existing unit tests with husky, do something like this...
```json
{
  "hooks": {
    "pre-push": "npm run test && npm run e2e"
  }
}
```